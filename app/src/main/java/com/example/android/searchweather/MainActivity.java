package com.example.android.searchweather;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

// Links to tutorials and helpful links
// https://www.androstock.com/tutorials/create-a-weather-app-on-android-android-studio.html
// https://blog.vikash.me/androidp-getlastlocationpp-returning-null/
// https://developer.android.com/reference/android/location/Location.html
// https://stackoverflow.com/questions/9873190/my-current-location-always-returns-null-how-can-i-fix-this
// https://firebase.google.com/docs/database/admin/retrieve-data#ordering-by-value
// https://stackoverflow.com/questions/20241857/android-intent-cannot-resolve-constructor
// https://stackoverflow.com/questions/45977847/make-sure-to-call-firebaseapp-initializeappcontext-first-in-android

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.android.MESSAGE";
    String TAG = "MainActivity";
    EditText searchText;

    // Will be grabbed from the user's search entry
    private String searchLatitude;
    private String searchLongitude;

    // Will be used to find current location
    private double lng = 0;
    private double lat = 0;

    TextView cityField;
    TextView detailsField;
    TextView currentTemperatureField;
    TextView humidity_field;
    TextView pressure_field;
    TextView weatherIcon;
    TextView updatedField;
    Typeface weatherFont;

    // Initialize our location services
    private FusedLocationProviderClient mFusedLocationCLient;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M ) {
            checkPermission();
        }
        this.searchText = (EditText) findViewById(R.id.searchText);



        // Assign the main UI to have current location's weather
        weatherFont = ResourcesCompat.getFont(this, R.font.weather);
        cityField = (TextView)findViewById(R.id.city_field);
        updatedField = (TextView)findViewById(R.id.updated_field);
        detailsField = (TextView)findViewById(R.id.details_field);
        currentTemperatureField = (TextView)findViewById(R.id.current_temperature_field);
        humidity_field = (TextView)findViewById(R.id.humidity_field);
        pressure_field = (TextView)findViewById(R.id.pressure_field);
        weatherIcon = (TextView)findViewById(R.id.weather_icon);
        weatherIcon.setTypeface(weatherFont);

        // Call my WeatherFunction class to asynchronously call the OpenWeatherAPI and get me that sweet, sweet data
        final WeatherFunction.placeIdTask asyncTask = new WeatherFunction.placeIdTask(new WeatherFunction.AsyncResponse() {

            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise) {

                cityField.setText(weather_city);
                updatedField.setText(weather_updatedOn);
                detailsField.setText(weather_description);
                currentTemperatureField.setText(weather_temperature);
                humidity_field.setText("Humidity: "+weather_humidity);
                pressure_field.setText("Pressure: "+weather_pressure);
                weatherIcon.setText(Html.fromHtml(weather_iconText));

            }
        });

        // Grab current longitude and latitude
        mFusedLocationCLient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationCLient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {

            @Override
            public void onSuccess(android.location.Location location) {
                if (location != null){
                    lng = location.getLongitude();
                    lat = location.getLatitude();
                    Toast toast = Toast.makeText(getBaseContext(), "Longitude: " + lng + "\nLatitude: " + lat, Toast.LENGTH_SHORT);
                    toast.show();
                    asyncTask.execute(String.valueOf(lat), String.valueOf(lng));

                } else {
                    Log.d(TAG, "Location is Null");
                }
            }
        });

    }

    // This method is atteched to the searchButton onClickListener
    public void sendZipcode(final View view) {
        String zipCode = searchText.getText().toString();

        // Dummy proof the search result
        boolean isInteger = false;
        boolean isLengthBig = false;
        boolean isLengthSmall = false;
        try {
            int num = Integer.parseInt(zipCode);
            isInteger = true;
            if (zipCode.length() > 5){
                isLengthBig = true;
            }
            if (zipCode.length() < 5) {
                isLengthSmall = true;
            }

        }catch (NumberFormatException ex) {
            Toast toast = Toast.makeText(view.getContext(), "Please input a valid Zip Code", Toast.LENGTH_SHORT);
            toast.show();

        }
        if (isInteger && !isLengthBig && !isLengthSmall) {

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference(zipCode);

            // Query Firebase
            ValueEventListener valueEventListener = ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {

                        // Use my Location object to attach to the Firebase JSON response
                        Location loc = dataSnapshot.getValue(Location.class);
                        searchLatitude = loc.latitude;
                        searchLongitude = loc.longitude;

                        // Start a new Intent and pass it my longitude and latitude variables
                        String[] locationArray = {loc.latitude, loc.longitude};
                        Intent intent = new Intent(view.getContext(), ViewWeather.class);
                        intent.putExtra(EXTRA_MESSAGE, locationArray);
                        startActivity(intent);
                    } else {
                        Toast toast = Toast.makeText(view.getContext(), "This Zip Code does not exist", Toast.LENGTH_SHORT);
                        toast.show();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (isLengthBig) {
            Toast toast = Toast.makeText(view.getContext(), "This Zip Code has too many digits", Toast.LENGTH_SHORT);
            toast.show();
        } else if (isLengthSmall) {
            Toast toast = Toast.makeText(view.getContext(), "This Zip Code needs more digits", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            Log.d(TAG, "isInteger: " + isInteger);
        }

    }

    // Used to make sure we have the right permissions in my Manifest
    public void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ){//Can add more as per requirement

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    123);
        }
    }

}
